FROM debian:jessie

ENV SAMTOOLS_VERSION=1.3.1

RUN set -ex \
&&  apt-get update -y \
&&  apt-get install -y \
    bash \
    ca-certificates \
    gcc \
    g++ \
    gzip \
    lbzip2 \
    make \
    ncurses-dev \
    tar \
    wget \
    zlib1g-dev

RUN mkdir /samtools/ 

# downloading samtools...
RUN cd /samtools/ \
&& wget --no-check-certificate https://sourceforge.net/projects/samtools/files/samtools/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2

## uncomment if needed:
# && wget --no-check-certificate https://sourceforge.net/projects/samtools/files/samtools/${SAMTOOLS_VERSION}/htslib-${SAMTOOLS_VERSION}.tar.bz2 \
# && wget --no-check-certificate https://sourceforge.net/projects/samtools/files/samtools/${SAMTOOLS_VERSION}/bcftools-${SAMTOOLS_VERSION}.tar.bz2

# unpacking samtools...
RUN cd /samtools/ \
&& tar xvf samtools-${SAMTOOLS_VERSION}.tar.bz2

## uncomment if needed:
# && tar xvf htslib-${SAMTOOLS_VERSION}.tar.bz2 \
# && tar xvf bcftools-${SAMTOOLS_VERSION}.tar.bz2

# compiling...
# samtools:
RUN cd /samtools/samtools-${SAMTOOLS_VERSION}/ \
&&  ./configure \
&&  make \
&&  make prefix=/samtools/samtools/ install
ENV PATH=/samtools/samtools/bin:$PATH

# cleanup:
RUN rm -rf /samtools/samtools-${SAMTOOLS_VERSION}

## bcftools:
# RUN cd /samtools/bcftools-${SAMTOOLS_VERSION}/ \
# &&  make \
# &&  make prefix=/samtools/bcftools/ install
# ENV PATH=/samtools/bcftools/bin:$PATH

## htslib:
# RUN cd /samtools/htslib-${SAMTOOLS_VERSION}/ \
# &&  make \
# &&  make prefix=/samtools/htslib/ install 
# ENV PATH=/samtools/htslib/bin:$PATH

CMD ['/bin/bash']


